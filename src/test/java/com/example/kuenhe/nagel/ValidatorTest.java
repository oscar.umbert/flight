package com.example.kuenhe.nagel;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;

import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.enums.ErrorsMessage;
import com.example.kuenhe.nagel.exception.FlightException;
import com.example.kuenhe.nagel.exception.ValidatorException;
import com.example.kuenhe.nagel.validator.Validator;
import com.example.kuenhe.nagel.validator.ValidatorChain;

public class ValidatorTest {

	private ValidatorChain validator = new Validator();
	
	@Test
	public void validateOk() throws ValidatorException {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("FASTEST");
		
		
		assertThat(validator.validate(flightDTO)).isTrue();
	}
	@Test
	public void validateErrorRequiredOrigin() {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("FASTEST");
		
		assertThatExceptionOfType(ValidatorException.class)
		  .isThrownBy(() -> {
			  validator.validate(flightDTO);
		}).withMessageMatching(ErrorsMessage.ORIGIN_DESTINATION_REQUIRED.getDescription());
		
	}
	@Test
	public void validateErrorRequiredDestination() {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("FASTEST");
		
		assertThatExceptionOfType(ValidatorException.class)
		  .isThrownBy(() -> {
			  validator.validate(flightDTO);
		}).withMessageMatching(ErrorsMessage.ORIGIN_DESTINATION_REQUIRED.getDescription());
		
	}
	@Test
	public void validateErrorRequiredWeight() {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BARCELONA");
		flightDTO.setWeight("");
		flightDTO.setSearchType("FASTEST");
		
		assertThatExceptionOfType(ValidatorException.class)
		  .isThrownBy(() -> {
			  validator.validate(flightDTO);
		}).withMessageMatching(ErrorsMessage.WEIGHT_REQUIRED.getDescription()+ErrorsMessage.WEIGHT_FORMAT.getDescription());
		
	}
	@Test
	public void validateErrorRequiredSearchType() {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("");
		
		assertThatExceptionOfType(ValidatorException.class)
		  .isThrownBy(() -> {
			  validator.validate(flightDTO);
		}).withMessageMatching(ErrorsMessage.SEARCH_REQUIRED.getDescription());
		
	}
	@Test
	public void validateErrorInvalidSearchType() {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("RRR");
		
		assertThatExceptionOfType(ValidatorException.class)
		  .isThrownBy(() -> {
			  validator.validate(flightDTO);
		}).withMessageMatching(ErrorsMessage.SEARCH_INVALID.getDescription());
		
	}
	@Test
	public void validateErrorFormatWeight() {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("fgd");
		flightDTO.setSearchType("FASTEST");
		
		assertThatExceptionOfType(ValidatorException.class)
		  .isThrownBy(() -> {
			  validator.validate(flightDTO);
		}).withMessageMatching(ErrorsMessage.WEIGHT_FORMAT.getDescription());
		
	}
	@Test
	public void validateErrorFormatOriginDestination() {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("4353454");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("fgd");
		flightDTO.setSearchType("FASTEST");
		
		assertThatExceptionOfType(ValidatorException.class)
		  .isThrownBy(() -> {
			  validator.validate(flightDTO);
		}).withMessageMatching(ErrorsMessage.WEIGHT_FORMAT.getDescription());
		
	}

}
