package com.example.kuenhe.nagel;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import com.example.kuenhe.nagel.controller.FlightController;
import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.enums.ErrorsMessage;
import com.example.kuenhe.nagel.exception.FlightException;
import com.example.kuenhe.nagel.exception.ValidatorException;
import com.example.kuenhe.nagel.model.Flight;
import com.example.kuenhe.nagel.service.FlightService;

public class FlightControllerTest {
	
	private FlightService flightService;
	private FlightController flightController;
	
	@BeforeEach
	public void initMocks() throws FlightException, ValidatorException { 

		this.flightService = createFlightServiceMock();
		this.flightController = new FlightController(this.flightService);
	}
	
	@Test
	public void getTestOk() throws FlightException, ValidatorException {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("FASTEST");
		assertThat(flightController.getFlight(flightDTO)).extracting("body").extracting("number").isEqualTo(1L);

	}
	@Test
	public void getTestError() throws FlightException, ValidatorException {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("FASTEST");
		this.flightService = createFlightServiceExcpetionMock();
		this.flightController = new FlightController(this.flightService);
		
		assertThatExceptionOfType(ValidatorException.class)
		.isThrownBy(() -> {
			  flightController.getFlight(flightDTO);
		}).withMessageMatching(ErrorsMessage.FLIGHT_NOT_EXIST.getDescription());
		

	}
	
	
	private FlightService createFlightServiceMock() throws FlightException, ValidatorException {
		FlightService flightService = Mockito.mock(FlightService.class);
		Mockito.when(flightService.get(Mockito.isA(FlightDTO.class))).thenReturn(new Flight(1L));
		return flightService;
	}
	private FlightService createFlightServiceExcpetionMock() throws FlightException, ValidatorException {
		FlightService flightService = Mockito.mock(FlightService.class);
		Mockito.when(flightService.get(Mockito.isA(FlightDTO.class))).thenThrow(new ValidatorException(ErrorsMessage.FLIGHT_NOT_EXIST.getDescription()));
		return flightService;
	}
}
