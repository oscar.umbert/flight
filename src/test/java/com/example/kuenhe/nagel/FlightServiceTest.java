package com.example.kuenhe.nagel;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.enums.ErrorsMessage;
import com.example.kuenhe.nagel.exception.FlightException;
import com.example.kuenhe.nagel.exception.ValidatorException;
import com.example.kuenhe.nagel.model.Flight;
import com.example.kuenhe.nagel.repository.FlightRepository;
import com.example.kuenhe.nagel.service.FlightService;
import com.example.kuenhe.nagel.validator.Validator;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class FlightServiceTest {

	private FlightService flightService;
	private FlightRepository flightRepository;
	private Validator validator;
	
	@BeforeEach
	public void initMocks() throws ValidatorException { 
		List<Flight> flights = Arrays.asList(new Flight(1L,"32423","BARCELONA","BERLIN",3,2,100),
											 new Flight(2L,"32545","BARCELONA","BERLIN",2,5,3000),
											 new Flight(3L,"5435","BARCELONA","BERLIN",1,8,2000),
											 new Flight(4L,"32545","BARCELONA","BERLIN",2,5,4500),
											 new Flight(5L,"35435","BARCELONA","BERLIN",7,1,1000),
											 new Flight(5L,"35435","BARCELONA","BERLIN",2,5,5000));

		this.flightRepository = createFlightRepositoryMock(flights);
		this.validator = createValidatorMock();
		this.flightService = new FlightService(this.flightRepository,this.validator);
	}
	
	@Test
	public void getFlightFastestOk() throws FlightException, ValidatorException {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("FASTEST");
		
		assertThat(flightService.get(flightDTO)).extracting("number").isEqualTo(5L);
	}
	@Test
	public void getFlightFastestEquals() throws FlightException, ValidatorException {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("3500");
		flightDTO.setSearchType("FASTEST");
		
		assertThat(flightService.get(flightDTO)).extracting("number").isEqualTo(4L);
	}
	@Test
	public void getFlightFastest2Ok() throws FlightException, ValidatorException {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("2500");
		flightDTO.setSearchType("FASTEST");
		
		assertThat(flightService.get(flightDTO)).extracting("number").isEqualTo(2L);
	}
	@Test
	public void getFlightCheapestOk() throws FlightException, ValidatorException {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("CHEAPEST");
				
		assertThat(flightService.get(flightDTO)).extracting("number").isEqualTo(5L);
	}
	@Test
	public void getFlightCheapest2Ok() throws FlightException, ValidatorException {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("1600");
		flightDTO.setSearchType("CHEAPEST");
				
		assertThat(flightService.get(flightDTO)).extracting("number").isEqualTo(2L);
	}
	@Test
	public void getFlightExceptionDestinationNotExists() {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BARCELONA");
		flightDTO.setDestination("BUENOS AIRES");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("CHEAPEST");
		assertThatExceptionOfType(FlightException.class)
		  .isThrownBy(() -> {
			  flightService.get(flightDTO);
		}).withMessageMatching(ErrorsMessage.FLIGHT_NOT_EXIST.getDescription());
		
	}
	@Test
	public void getFlightExceptionOriginNotExists() {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BUENOS AIRES");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("CHEAPEST");
		assertThatExceptionOfType(FlightException.class)
		  .isThrownBy(() -> {
			  flightService.get(flightDTO);
		}).withMessageMatching(ErrorsMessage.FLIGHT_NOT_EXIST.getDescription());
		
	}
	@Test
	public void getFlightExceptionValidation() throws ValidatorException {
		FlightDTO flightDTO = new FlightDTO();
		flightDTO.setOrigin("BUENOS AIRES");
		flightDTO.setDestination("BERLIN");
		flightDTO.setWeight("90");
		flightDTO.setSearchType("CHEAPEST");
		this.flightService = new FlightService(null,createValidatorExceptionMock());
		assertThatExceptionOfType(ValidatorException.class)
		  .isThrownBy(() -> {
			  flightService.get(flightDTO);
		}).withMessageMatching(ErrorsMessage.FLIGHT_NOT_EXIST.getDescription());
		
	}
	private FlightRepository createFlightRepositoryMock(List<Flight> flights) {
		FlightRepository flightRepository = Mockito.mock(FlightRepository.class);
		Mockito.when(flightRepository.findAll()).thenReturn(flights);
		return flightRepository;
	} 
	private Validator createValidatorMock() throws ValidatorException {
		Validator validator = Mockito.mock(Validator.class);
		Mockito.when(validator.validate(Mockito.isA(FlightDTO.class))).thenReturn(true);
		return validator;
	} 
	private Validator createValidatorExceptionMock() throws ValidatorException {
		Validator validator = Mockito.mock(Validator.class);
		Mockito.when(validator.validate(Mockito.isA(FlightDTO.class))).thenThrow(new ValidatorException(ErrorsMessage.FLIGHT_NOT_EXIST.getDescription()));
		return validator;
	} 
	
}
