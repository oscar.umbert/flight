package com.example.kuenhe.nagel.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.enums.ErrorsMessage;
import com.example.kuenhe.nagel.enums.SearchType;
import com.example.kuenhe.nagel.exception.FlightException;
import com.example.kuenhe.nagel.exception.ValidatorException;
import com.example.kuenhe.nagel.model.Flight;
import com.example.kuenhe.nagel.repository.FlightRepository;
import com.example.kuenhe.nagel.validator.Validator;
import com.example.kuenhe.nagel.validator.ValidatorChain;

@Service
public class FlightService {
	
	@Autowired
	private FlightRepository flightRepository; 
	
	@Autowired
	private ValidatorChain validator;
	
	Comparator<Flight> comparatorPriceWeight = Comparator.comparing( Flight::getPriceByWeight);
	Comparator<Flight> comparatorTime = Comparator.comparing( Flight::getPriceByWeight);


	public FlightService(FlightRepository flightRepository,Validator validator) {
		this.flightRepository = flightRepository;
		this.validator = validator;
	}
	
	public Flight get(FlightDTO flightDto) throws FlightException, ValidatorException {
		Flight flight = null;
		
		validator.validate(flightDto);
		
		List<Flight> flights = findByFilters(flightDto.getOrigin(),
											 flightDto.getDestination(),
											 Integer.parseInt(flightDto.getWeight()));
		
		Optional<Flight> optional = searchByType(SearchType.valueOf(flightDto.getSearchType()),flights);
		
		if(optional.isPresent()) {
			flight = optional.get();
		}else {
			throw new FlightException(ErrorsMessage.FLIGHT_NOT_EXIST);
		}
		
		return flight;
		
	}
	private List<Flight> findByFilters(String origin,String destination,Integer weight){
		List<Flight> flights = this.flightRepository.findAll().stream()
				.filter(p -> p.getOrigin().equals(origin)
						&& p.getDestination().equals(destination)
						&& Integer.compare(p.getWeightAvailables(), weight) > 0)
				.collect(Collectors.toList());
		
		return flights;
		
	}
	private Optional<Flight> searchByType(SearchType searchType,List<Flight> flights) {
		
		Optional <Flight> optional = flights.stream().min(searchType.equals(searchType.CHEAPEST) ? comparatorPriceWeight : comparatorTime);
		
		return optional;

	}
}
