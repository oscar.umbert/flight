package com.example.kuenhe.nagel.exception;

import com.example.kuenhe.nagel.enums.ErrorsMessage;

/**
 * This is thrown if the flight not exist
 * @author oscar
 *
 */
public class FlightException extends Exception{

	private ErrorsMessage errorsMessage;
	
	public FlightException(ErrorsMessage errorsMessage) {
		super(errorsMessage.getDescription());
	}

	
}
