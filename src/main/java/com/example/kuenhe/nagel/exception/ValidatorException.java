package com.example.kuenhe.nagel.exception;

import java.util.List;

import com.example.kuenhe.nagel.enums.ErrorsMessage;
/**
 * This is thrown if the query data does not pass validation
 * @author oscar
 *
 */
public class ValidatorException extends Exception{

	private List<ErrorsMessage> errorsMessage;
	
	public ValidatorException(String messages) {
		super(messages);
		
		
	}
}
