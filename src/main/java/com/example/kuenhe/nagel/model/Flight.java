package com.example.kuenhe.nagel.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Flight {
	@Id
	private Long number;
	private String airlineCode;
	private String origin;
	private String destination;
	private Integer time;
	private Integer priceByWeight;
	private Integer weightAvailables;
	
	public String getAirlineCode() {
		return airlineCode;
	}
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}
	public Long getNumber() {
		return number;
	}
	public void setNumber(Long number) {
		this.number = number;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public Integer getTime() {
		return time;
	}
	public void setTime(Integer time) {
		this.time = time;
	}
	public Integer getPriceByWeight() {
		return priceByWeight;
	}
	public void setPriceByWeight(Integer priceByWeight) {
		this.priceByWeight = priceByWeight;
	}
	public Integer getWeightAvailables() {
		return weightAvailables;
	}
	public void setWeightAvailables(Integer weightAvailables) {
		this.weightAvailables = weightAvailables;
	}
	public Flight() {
		
	}
	public Flight(Long number) {
		this.number = number;
	}
	public Flight(Long number, String airlineCode, String origin, String destination, Integer time,
			Integer priceByWeight, Integer kgAvailables) {
		super();
		this.number = number;
		this.airlineCode = airlineCode;
		this.origin = origin;
		this.destination = destination;
		this.time = time;
		this.priceByWeight = priceByWeight;
		this.weightAvailables = kgAvailables;
	}
	
	
	
	
}
