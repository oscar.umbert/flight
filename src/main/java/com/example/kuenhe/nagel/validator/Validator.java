package com.example.kuenhe.nagel.validator;

import org.springframework.stereotype.Component;

import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.exception.ValidatorException;
@Component
public class Validator implements ValidatorChain{

	private ValidatorChain next;
	@Override
	public void setNext(ValidatorChain validator) {
		next = validator;
	}

	@Override
	public ValidatorChain getNext() {
		return next;
	}

	@Override
	public boolean validate(FlightDTO flight) throws ValidatorException {
		ValidateOriginDestination validatorOriginDestinacion = new ValidateOriginDestination();
		this.setNext(validatorOriginDestinacion);
		
		ValidateWeight validateWeight = new ValidateWeight();
		validatorOriginDestinacion.setNext(validateWeight);
		
		ValidateSearchType validateSearchType = new ValidateSearchType();
		validateWeight.setNext(validateSearchType);
		
		return next.validate(flight);
		
	}

	@Override
	public String validateRequired(String valueOne, String valueTwo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String validateFormat(String valueOne, String valueTwo) {
		// TODO Auto-generated method stub
		return null;
	}


}
