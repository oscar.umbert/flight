package com.example.kuenhe.nagel.validator;

import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.enums.ErrorsMessage;
import com.example.kuenhe.nagel.enums.SearchType;
import com.example.kuenhe.nagel.exception.ValidatorException;

public class ValidateSearchType implements ValidatorChain{

	private ValidatorChain next;
	
	@Override
	public void setNext(ValidatorChain validator) {
		next = validator;
	}

	@Override
	public ValidatorChain getNext() {
		return next;
	}

	@Override
	public boolean validate(FlightDTO flight) throws ValidatorException {
		String message = new String();
		if(flight.getSearchType() == null || flight.getSearchType().equals("")) {
			message =  ErrorsMessage.SEARCH_REQUIRED.getDescription();
		}else {
			try {
				SearchType.valueOf(flight.getSearchType());
			}catch(Exception e) {
				throw new ValidatorException(ErrorsMessage.SEARCH_INVALID.getDescription());
			}
		}
		if(!message.isEmpty()) {
			throw new ValidatorException(message);

		}
		return true;
		
		
	}

	@Override
	public String validateRequired(String valueOne, String valueTwo) {
		return valueOne == null || valueOne.equals("") ? ErrorsMessage.SEARCH_REQUIRED.getDescription() : "";
	}

	@Override
	public String validateFormat(String valueOne, String valueTwo) {
		// TODO Auto-generated method stub
		return null;
	}

}
