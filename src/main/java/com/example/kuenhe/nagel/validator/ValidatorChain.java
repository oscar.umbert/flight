package com.example.kuenhe.nagel.validator;

import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.exception.ValidatorException;

public interface ValidatorChain {
	void setNext(ValidatorChain validator);
	ValidatorChain getNext();
    boolean validate(FlightDTO flight)throws ValidatorException;
	String validateRequired(String valueOne,String valueTwo);
	String validateFormat(String valueOne,String valueTwo);


}
