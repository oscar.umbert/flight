package com.example.kuenhe.nagel.validator;

import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.enums.ErrorsMessage;
import com.example.kuenhe.nagel.exception.ValidatorException;

public class ValidateWeight implements ValidatorChain{

	private ValidatorChain next;
 
	@Override
	public void setNext(ValidatorChain validator) {
		next = validator;
	}

	@Override
	public ValidatorChain getNext() {
		return next;
	}

	@Override
	public boolean validate(FlightDTO flight) throws ValidatorException {
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append(validateRequired(flight.getWeight(),null));
		stringBuilder.append(validateFormat(flight.getWeight(),null));
	
		if(stringBuilder.toString().isEmpty()) {
			return next.validate(flight);
		}else {			
			throw new ValidatorException(stringBuilder.toString());
		}
		
	}

	@Override
	public String validateRequired(String valueOne, String valueTwo) {
		return valueOne == null || valueOne.equals("") ? ErrorsMessage.WEIGHT_REQUIRED.getDescription() : "";
	}

	@Override
	public String validateFormat(String valueOne, String valueTwo) {
		return !valueOne.matches("[0-9].*") ? ErrorsMessage.WEIGHT_FORMAT.getDescription() : "";
	}



}
