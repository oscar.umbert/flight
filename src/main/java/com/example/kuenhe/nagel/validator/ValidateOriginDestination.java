package com.example.kuenhe.nagel.validator;

import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.enums.ErrorsMessage;
import com.example.kuenhe.nagel.exception.ValidatorException;

public class ValidateOriginDestination implements ValidatorChain{

	private ValidatorChain next;
	private StringBuilder stringBuilder;
	
	@Override
	public void setNext(ValidatorChain validator) {
		next = validator;
		
	}
	@Override
	public ValidatorChain getNext() {
		return next;
	}

	@Override
	public boolean validate(FlightDTO flight) throws ValidatorException {
		StringBuilder stringBuilder = new StringBuilder();
		

		stringBuilder.append(validateRequired(flight.getOrigin(),flight.getDestination()));
		stringBuilder.append(validateFormat(flight.getOrigin(),flight.getDestination()));
		
		if(stringBuilder.toString().isEmpty()) {
			return next.validate(flight);
		}else {			
			throw new ValidatorException(stringBuilder.toString());
		}
		
	}
	
	public String validateRequired(String valueOrigin,String valueDestination) {
		return valueOrigin == null || valueOrigin.equals("") || 
			   valueDestination == null || valueDestination.equals("") ?  ErrorsMessage.ORIGIN_DESTINATION_REQUIRED.getDescription() : "";
		
	}
	public String validateFormat(String valueOrigin,String valueDestination) {
		return !valueOrigin.matches("[a-zA-Z].*") && 
			   !valueDestination.matches("[a-zA-Z].*") ?  ErrorsMessage.ORIGIN_DESTINATION_FORMAT.getDescription() : "";
		
	}



}
