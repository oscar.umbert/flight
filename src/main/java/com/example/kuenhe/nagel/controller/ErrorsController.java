package com.example.kuenhe.nagel.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.kuenhe.nagel.exception.FlightException;
import com.example.kuenhe.nagel.exception.ValidatorException;

/**
 * This class catch all the exceptions
 * @author oscar
 *
 */
@RestControllerAdvice
public class ErrorsController {
	
	@ExceptionHandler(FlightException.class)
	public ResponseEntity<?> handleTrackerException(FlightException e){
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
	}
	@ExceptionHandler(ValidatorException.class)
	public ResponseEntity<?> handleTrackerException(ValidatorException e){
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
	}
}
