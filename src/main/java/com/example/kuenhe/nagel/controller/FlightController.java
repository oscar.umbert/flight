package com.example.kuenhe.nagel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.kuenhe.nagel.dto.FlightDTO;
import com.example.kuenhe.nagel.exception.FlightException;
import com.example.kuenhe.nagel.exception.ValidatorException;
import com.example.kuenhe.nagel.service.FlightService;

@RestController
public class FlightController {

	public FlightController(FlightService flightService) {
		this.flightService = flightService;
	}
	@Autowired
	private FlightService flightService;
	
	@GetMapping(value ="/flight")
	public ResponseEntity<?> getFlight(@RequestBody FlightDTO flightDto) throws FlightException, ValidatorException {
		
		return ResponseEntity.status(HttpStatus.OK).body(flightService.get(flightDto)); 

	}
	
}
