package com.example.kuenhe.nagel.enums;

import org.springframework.http.HttpStatus;

public enum ErrorsMessage {
	
	FLIGHT_NOT_EXIST("El vuelo que busca no existe",HttpStatus.OK),
	ORIGIN_DESTINATION_FORMAT("El origin y destino debe contener solo letras",HttpStatus.BAD_REQUEST),
	ORIGIN_DESTINATION_REQUIRED("El origin y destino es obligatorio",HttpStatus.BAD_REQUEST),
	WEIGHT_FORMAT("El peso del vuelo debe ser numerico",HttpStatus.BAD_REQUEST),
	WEIGHT_REQUIRED("El peso del vuelo es obligatorio",HttpStatus.BAD_REQUEST),
	SEARCH_REQUIRED("El tipo de busqueda es obligatorio",HttpStatus.BAD_REQUEST),
	SEARCH_INVALID("El tipo de busqueda ingreso es invalido solo puede ser FASTEST o CHEAPEST", HttpStatus.BAD_REQUEST);

	private String description;
	private HttpStatus status;
	
	private ErrorsMessage(String description,HttpStatus status) {
		this.description = description;
		this.status = status;
	}

	public String getDescription() {
		return description;
	}
}
