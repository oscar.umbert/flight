package com.example.kuenhe.nagel.enums;
/**
 * This enum define the type of search.
 * There are two types: CHAPEST: this is the flight cheapest
 * 						FASTEST: this is the flight fastest
 * @author oscar
 *
 */
public enum SearchType {
	CHEAPEST,FASTEST
}
