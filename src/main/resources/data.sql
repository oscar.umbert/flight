DROP TABLE IF EXISTS flight;

CREATE TABLE flight (
  number INT PRIMARY KEY,
  airline_code VARCHAR(250) NOT NULL,
  origin VARCHAR(250) NOT NULL,
  destination VARCHAR(250) NOT NULL,
  time INT,
  price_by_weight INT,
  weight_availables INT
);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(1,'3040','BARCELONA','LONDRES',2,5,100);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(2,'3040','BARCELONA','LONDRES',3,4,200);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(3,'2343','BARCELONA','LONDRES',5,5,300);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(4,'5543','BARCELONA','LONDRES',6,5,200);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(5,'1111','MADRID','ROMA',4,8,300);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(6,'5543','MADRID','ROMA',4,25,400);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(7,'2343','MADRID','ROMA',2,25,200);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(8,'5543','MADRID','ROMA',4,5,200);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(9,'1111','MANCHESTER','DUBLIN',4,25,1000);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(10,'2343','MANCHESTER','DUBLIN',1,25,1000);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(11,'3040','MANCHESTER','DUBLIN',2,3,100);
insert into flight (number,airline_code,origin,destination,time,price_by_weight,weight_availables)values(12,'2343','MANCHESTER','DUBLIN',3,5,100);